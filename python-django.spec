%global _empty_manifest_terminate_build 0
Name:		python-django
Version:	4.2.3
Release:	1
Summary:	A high-level Python Web framework that encourages rapid development and clean, pragmatic design.
License:	Apache-2.0 and Python-2.0 and BSD-3-Clause
URL:		https://www.djangoproject.com/
Source0:        https://files.pythonhosted.org/packages/36/24/d0e78e667f98efcca76c8b670ef247583349a8f5241cdb3c98eeb92726ff/Django-4.2.3.tar.gz

BuildArch:	noarch
%description
A high-level Python Web framework that encourages rapid development and clean, pragmatic design.


%package -n python3-Django
Summary:	A high-level Python Web framework that encourages rapid development and clean, pragmatic design.
Provides:	python-Django, python3-django
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:	python3-pytz
Requires:	python3-sqlparse
Requires:	python3-argon2-cffi
Requires:	python3-bcrypt
%description -n python3-Django
A high-level Python Web framework that encourages rapid development and clean, pragmatic design.


%package help
Summary:	Development documents and examples for Django
Provides:	python3-Django-doc
%description help
Development documents and examples for Django

%prep
%autosetup -n Django-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-Django -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Jul 11 2023 chenzixuan <chenzixuan@kylinos.cn> - 4.2.3-1
- Update to 4.2.3

* Tue May 16 2023 yaoxin <yao_xin001@hoperun.com> - 4.1.7-2
- Fix CVE-2023-31047

* Tue Apr 11 2023 yaoxin <yao_xin001@hoperun.com> - 4.1.7-1
- Update to 4.1.7

* Sat Feb 25 2023 yaoxin <yaoxin30@h-partners.com> - 4.1.4-3
- Fix CVE-2023-24580

* Mon Feb 13 2023 yaoxin <yaoxin30@h-partners.com> - 4.1.4-2
- Fix CVE-2023-23969

* Fri Dec 09 2022 chendexi <chendexi@kylinos.cn> - 4.1.4-1
- Upgrade package to version 4.1.4

* Tue Aug 09 2022 huangduirong <huangduirong@huawei.com> - 3.2.12-3
- Type: bugfix
- CVE: CVE-2022-36359
- SUG: NA
- DESC: Fix CVE-2022-36359

* Tue Jul 05 2022 yaoxin <yaoxin30@h-partners.com> - 3.2.12-2
- Fix CVE-2022-34265

* Wed May 18 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 3.2.12-1
- Update to 3.2.12

* Thu Apr 21 2022 yaoxin <yaoxin30@h-partners.com> - 2.2.27-2
- Fix CVE-2022-28346 CVE-2022-28347

* Thu Feb 10 2022 houyingchao <houyingchao@huawei.com> - 2.2.27-1
- Upgrade to 2.2.27
- Fix CVE-2021-45115 CVE-2021-45116 CVE-2021-45452 CVE-2022-22818 CVE-2022-23833

* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Update to 2.2.19

* Tus Feb 09 2021 wangxiyuan <wangxiyuan1007@gmail.com>
- Add python3-django for correct package name. Due to backward compatibility, the python3-Django is kept and it should be removed in the future.

* Fri Jan 08 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
